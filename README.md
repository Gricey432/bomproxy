# bomproxy
A serverless proxy to the Bureau of Meteorology's FTP radar data

Deployed on AWS Lambda, the script simply connects to the BOM's ftp server and gets the latest image for a given location.

Lots of things could be added, especially more locations.
