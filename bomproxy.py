from ftplib import FTP
import base64
from io import BytesIO
import time

import boto3
import botocore


CACHE_BUCKET_NAME = "bomproxy-cache"

LOCATIONS = {
    'brisbane': 'IDR503',  # 'IDR663' maintenance until late sept
    'sydney': 'IDR713'
}


s3 = boto3.resource("s3")
bucket = s3.Bucket(CACHE_BUCKET_NAME)


def s3_key(location_code, timestamp):
    return "{}/{}.gif".format(location_code, timestamp)


def get_from_s3_if_exists(location_code, timestamp):
    key = s3_key(location_code, timestamp)
    try:
        file_like = BytesIO()
        bucket.download_fileobj(key, file_like)
        file_like.seek(0)
        return file_like
    except botocore.exceptions.ClientError as e:
        print("Key {} not in bucket".format(key))
        return None


def put_to_s3(file_like, location_code, timestamp):
    key = s3_key(location_code, timestamp)
    file_like.seek(0)
    bucket.upload_fileobj(file_like, key)
    file_like.seek(0)


def get_from_bom(location_code):
    ftp = FTP('ftp.bom.gov.au')
    ftp.login()

    img = BytesIO()
    ftp.retrbinary('RETR /anon/gen/radar/{}.gif'.format(location_code), lambda x: img.write(x))
    img.seek(0)

    ftp.close()
    return img


def notfound():
    return {
        'statusCode': '404',
        'body': 'Not Found',
    }


def lambda_handler(event, context):
    qsp = event['queryStringParameters']
    location_name = qsp['location']
    timestamp = int(qsp['timestamp']) if 'timestamp' in qsp else None
    location_code = LOCATIONS.get(location_name)
    if not location_code:
        return notfound()

    # Ensure the provided timestamp actually is reasonably close to the real time
    # Otherwise people could cache anything on any integer
    use_cache = False
    if timestamp:
        real_timestamp = int(time.time())
        if abs(timestamp - real_timestamp) < 60:
            # Ok
            use_cache = True

    # Fetch image from cache if using
    img = None
    if use_cache:
        img = get_from_s3_if_exists(location_code, timestamp)

    # Fetch image from BOM if not from cache
    if img is None:
        img = get_from_bom(location_code)

        # Put to cache if caching
        if use_cache:
            put_to_s3(img, location_code, timestamp)

    return {
        'statusCode': '200',
        'body': base64.b64encode(img.read()),
        'isBase64Encoded': True,
        'headers': {
            'Content-Type': 'image/gif',
        },
    }
